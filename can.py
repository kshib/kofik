import json
import time
import sys
import requests


# round float to two places
def format_decimals(decimal):
    return '{:3.2f}'.format(round(decimal, 2))


arg = sys.argv[1]

# urls
screener_url = "https://api.tickertape.in/screener/query"
ticker_resolver_url = "https://api.tickertape.in/stockwidget/internal/{}"

# let tickertape.in know where the request is coming from
user_agent = {"User-Agent": "github.com/ksyko/kofik"}

# read payload from file
with open(f"payload_{arg}.json", "r") as payload_file:
    payload_read = payload_file.read()
payload = json.loads(payload_read)

# make request
response = requests.post(screener_url, json=payload, headers=user_agent).json()

# find which stocks match the criteria
for result in response["data"]["results"]:
    stock_id = result["sid"]
    stock_ratios = result["stock"]["advancedRatios"]
    stock_info = result["stock"]["info"]
    stock = stock_info["name"]
    ticker = stock_info["ticker"]
    cap = stock_ratios["mrktCapf"]
    rev = stock_ratios["5YrevChg"]
    roe = stock_ratios["5Yroe"]
    roc = stock_ratios["roce"]

    # ignore stocks with no cap, rev or roe
    if cap is None or rev is None or roe is None:
        continue

    # print stocks matching the strategy criteria
    if (int(cap) > 500 and int(rev) > 10) and int(roe) > 15:
        response = requests.get(ticker_resolver_url.format(stock_id), headers=user_agent).json()
        slug = response["data"]["info"]["slug"]
        print(stock)
        print("https://www.tickertape.in" + slug)
        print(format_decimals(rev) + "%", "Revenue growth (5 years)")
        print(format_decimals(roe) + "%", "Return on equity (5 years)")
        print(format_decimals(roc) + "%", "Return on Capital employed")
        print("\n")
        time.sleep(2)
