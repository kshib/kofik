# kofik
Find stocks for long term investment, based on 'Coffee Can Investing'.
The script uses [tickertape screener](https://www.tickertape.in/screener) to find which stocks fulfill the criteria.

Original idea:
- [Coffee Can Investing by Saurabh Mukherjea](https://www.amazon.com/Coffee-Can-Investing-Stupendous-Wealth/dp/067009045X)

Video based on the same:
- [How to pick stocks in 10 minutes by Tanmay Bhat](https://youtu.be/dwuAKbh5PLs) and [Anshuman Sharma](https://twitter.com/Anshuman_306)

screener.in:
- [Coffee Can Portfolio](https://www.screener.in/screens/57601/coffee-can-portfolio/)

## Requirements
[Python 3](https://www.python.org/downloads/)

## Installation
```
git clone https://github.com/ksyko/kofik.git
cd kofik
pip install requests
```

## Usage
For NIFTY 50 and NIFTY 100 respectively
```
python can.py n50
python can.py n100
```

## Sample output
```
HDFC Bank Ltd
https://www.tickertape.in/stocks/hdfc-bank-HDBK
15.95% Revenue growth (5 years)
17.30% Return on equity (5 years)
9.19% Return on Capital employed


Bajaj Finance Ltd
https://www.tickertape.in/stocks/bajaj-finance-BJFN
29.47% Revenue growth (5 years)
19.33% Return on equity (5 years)
3.89% Return on Capital employed


HCL Technologies Ltd
https://www.tickertape.in/stocks/hcl-technologies-HCLT
18.98% Revenue growth (5 years)
24.65% Return on equity (5 years)
23.83% Return on Capital employed

```
